from gradient_finder import GradientFinder
import cv2


solver = GradientFinder('samples/sample_3.jpg')
solver.showGradientAreas()

solver.loadImageFrom('samples/sample_1.jpg')
rectangles = solver.findRectangles()
result_img = solver.getGradientAreas()
cv2.imwrite('sample_result.jpg', result_img)
solver.showGradientAreas()
