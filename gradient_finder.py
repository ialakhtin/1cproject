import cv2
import numpy as np

class GradientFinder:
    __MAX_GRAD = 20

    def __to_default(self):
        self.__rectangles = []  # (x_left, x_right, y_left, y_right)
        self.__img_with_rectangles = None
        self.__rect_found = False
        self.__rect_drown = False

    def __init__(self, file_path=None, min_side_len=30, rectangle_width=3, max_rect_count=1):
        self.__img = None
        if file_path is not None:
            self.loadImageFrom(file_path)
        self.__to_default()
        self.__min_side_len = min_side_len
        self.__rectangle_width = rectangle_width
        self.__max_rect_count = max_rect_count

    #передает в класс уже загруженное RGB изображение
    def loadImage(self, img):
        self.__img = img
        self.__to_default()

    #загружает изображение из файла
    def loadImageFrom(self, file_path):
        self.__img = cv2.imread(file_path)
        self.__to_default()

    @staticmethod
    def __lowerOnLeft(hist):
        max_len = np.zeros(hist.shape, dtype=int)
        stack = []
        stack.append([-1, -1])
        for i in range(len(hist)):
            while len(stack) > 0 and stack[-1][0] >= hist[i]:
                stack.pop()
            max_len[i] = i - stack[-1][1]
            stack.append([hist[i], i])
        return max_len

    def __findMaxHistArea(self, hist):
        left_len = GradientFinder.__lowerOnLeft(hist)
        right_len = GradientFinder.__lowerOnLeft(hist[::-1])[::-1]
        max_area = -1
        index = -1
        for i in range(len(hist)):
            valid_len = left_len[i] + right_len[i] - 1
            if valid_len * hist[i] > max_area and min(hist[i], valid_len) >= self.__min_side_len:
                max_area = valid_len * hist[i]
                index = i
        if index == -1:
            return -1, -1, None, None
        return max_area, index, index - left_len[index] + 1, index + right_len[index]

    def __findMaxAreaRect(self, valid_cells):
        old_hist = []
        max_area = -1
        rectangle = None
        for x in range(len(valid_cells)):
            new_hist = np.zeros(valid_cells[x].shape, dtype=int)
            for y in range(len(new_hist)):
                new_hist[y] = 1 if valid_cells[x][y] else 0
                if x > 0 and new_hist[y] == 1:
                    new_hist[y] += old_hist[y]
            max_hist_area, index, left_y, right_y = self.__findMaxHistArea(new_hist)
            if max_hist_area > max_area:
                max_area = max_hist_area
                rectangle = (x - new_hist[index] + 1, x + 1, left_y, right_y)
            old_hist = new_hist
        return rectangle

    def __findGrad(self):
        layers = np.transpose(self.__img, (2, 0, 1))
        sobel_x = np.array([[-1, -2, -1], [0, 0, 0], [1, 2, 1]])
        sobel_y = sobel_x.T
        img_grad_x = np.array(list(map((lambda layer: cv2.filter2D(layer, cv2.CV_64F, sobel_x)), layers)))
        img_grad_y = np.array(list(map((lambda layer: cv2.filter2D(layer, cv2.CV_64F, sobel_y)), layers)))
        img_grad = np.sum(((img_grad_x ** 2 + img_grad_y ** 2) / 6), axis=0) ** 0.5
        sobel_mean = np.ones((3, 3)) / 9
        return cv2.filter2D(img_grad, -1, sobel_mean)

    #возвращает list из найденных прямоугольников
    #в формате (x_left, x_right, y_left, y_right)
    def findRectangles(self):
        if self.__rect_found:
            return self.__rectangles
        self.__rect_found = True
        img_grad = self.__findGrad()
        valid_cells = img_grad < GradientFinder.__MAX_GRAD
        for i in range(self.__max_rect_count):
            rectangle = self.__findMaxAreaRect(valid_cells)
            if rectangle is None:
                break
            self.__rectangles.append(rectangle)
            x_left, x_right, y_left, y_right = rectangle
            for x in range(x_left, x_right):
                for y in range(y_left, y_right):
                    valid_cells[x][y] = False
        return self.__rectangles

    def __drawLine(self, y, x_left, x_right, orientation='h'):
        for x in range(x_left, x_right):
            for y_delta in range(self.__rectangle_width):
                if orientation == 'h':
                    self.__img_with_rectangles[x][y + y_delta] = [0, 255, 0]
                else:
                    self.__img_with_rectangles[y + y_delta][x] = [0, 255, 0]

    def __drawRectangles(self):
        if self.__rect_drown:
            return
        self.__rect_drown = True
        self.__img_with_rectangles = self.__img.copy()
        height, width, _ = self.__img_with_rectangles.shape
        for x_left, x_right, y_left, y_right in self.__rectangles:
            x_right = min(height - self.__rectangle_width, x_right - 1)
            y_right = min(width - self.__rectangle_width, y_right - 1)
            self.__drawLine(y_left, x_left, x_right + self.__rectangle_width, 'h')
            self.__drawLine(y_right, x_left, x_right + self.__rectangle_width, 'h')
            self.__drawLine(x_left, y_left, y_right + self.__rectangle_width, 'v')
            self.__drawLine(x_right, y_left, y_right + self.__rectangle_width, 'v')

    #возвращает изображение с нарисованными прямоугольниками
    def getGradientAreas(self):
        self.findRectangles()
        self.__drawRectangles()
        return self.__img_with_rectangles

    #создает окно, в котором рисует изображение с прямоугольниками
    def showGradientAreas(self):
        self.getGradientAreas()
        height, width, _ = self.__img_with_rectangles.shape
        cv2.namedWindow('Gradient Areas', cv2.WINDOW_KEEPRATIO)
        cv2.imshow('Gradient Areas', self.__img_with_rectangles)
        cv2.resizeWindow('Gradient Areas', width // 2, height // 2)
        cv2.waitKey(0)
        cv2.destroyAllWindows()
